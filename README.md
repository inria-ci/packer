# packer

This repository contains [`Packer`](https://packer.io) specifications
for some ci.inria.fr templates.

All templates share the following properties:

- there is a user `ci`, with password `ci`, with `/builds` for home directory,
- there is an SSH server, with keys generated on first boot, which
  allows password-based authentication,
- `build-essential` (make, gcc, ...) is installed,
- `gitlab-runner` is installed, and `gitlab-runner` UID/GID are set to 995/995,
- NFS client is installed,
- the root partition is resized automatically at first boot to fill the
  actual root disk size.
  The initial root partitions are intentionally small (3 GB for Alpine and
  9 GB for Ubuntu) to allow users to create VMs with small footprint,
- `motd-news` is disabled,
- `/etc/update-motd.d/30-security-warning` to ask the user to change the default passwords.

## Ubuntu variants

Ubuntu variants share the following properties:

- [`cloud-init`](https://cloud-init.io) is enabled with CloudStack as data
  source,
- `default-jre` package is installed.

`cloud-init` is used for automatic installation and is activated again
to run when virtual machines created from these templates are
instantiated. `cloud-init` is responsible for regenerating SSH server
keys.

To activate `cloud-init` again after automatic installation, the
following steps are performed:
- remove the arguments
  ` autoinstall 'ds=nocloud-net;s=http://{{ .HTTPIP }}:{{ .HTTPPort }}/'`
  from the kernel command line in `/boot/grub/grub.cfg`:
  these arguments are inherited from the automatic installation setup
  and force `cloud-init` to consider only the data source `nocloud-net`,
- run `sudo cloud-init clean --machine-id`.

### ci.inria.fr/ubuntu-24.04.1/amd64

This variant is based on the ISO image
[Ubuntu 24.04.1 (Noble) Live Server for amd64].

[Ubuntu 24.04.1 (Noble) Live Server for amd64]: https://releases.ubuntu.com/noble/ubuntu-24.04.1-live-server-amd64.iso

### ci.inria.fr/ubuntu-23.04/amd64

This variant is based on the ISO image
[Ubuntu 23.04 (Lunar) Live Server for amd64].

[Ubuntu 23.04 (Lunar) Live Server for amd64]: https://releases.ubuntu.com/lunar/ubuntu-23.04-live-server-amd64.iso

### ci.inria.fr/ubuntu-22.04/amd64

This variant is based on the ISO image
[Ubuntu 22.04.2 (Jammy) Live Server for amd64].

[Ubuntu 22.04.2 (Jammy) Live Server for amd64]: https://releases.ubuntu.com/jammy/ubuntu-22.04.2-live-server-amd64.iso

## Alpine variants

### ci.inria.fr/alpine-3.18.2/amd64

This template is based on the ISO image [Alpine standard 3.18.2 for x86_64].

[Alpine standard 3.18.2 for x86_64]: https://dl-cdn.alpinelinux.org/alpine/v3.18/releases/x86_64/alpine-standard-3.18.2-x86_64.iso

The following packages are pre-installed: `docker`, `openrc`, `sudo`,
`curl`, `bash`, `shadow`, `git`, `gcompat`. The package
`openjdk17-jre` is installed in this variant as well.

`gitlab-runner` is installed.

`cloud-init` is not installed: I didn't manage to make `cloud-init`
work with Alpine Linux.

### ci.inria.fr/alpine-3.18.2-runner/amd64

This variant is similar to `ci.inria.fr/alpine-3.18.2/amd64`, but
without `openjdk17-jre`, to save 190 MB disk space.
