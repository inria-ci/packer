source "qemu" "alpine" {
  accelerator = "kvm"
  iso_url = "alpine-3.18.2.iso"
  iso_checksum = "513dfc3c0c516b803095cd829df72cea956757316247584f0f2ddde7e5b0262f"
  ssh_username = "root"
  ssh_password = "password"
  ssh_timeout  = "40m"
  disk_size = 3000
  vm_name = "alpine-3.18.2.qcow2"
  headless = true
  boot_command  = [
    "<wait30s>",
    "root<enter>",
    "setup-alpine -q<enter><wait5s>",
    "us us<enter><wait20s>",
    "passwd<enter>password<enter>password<enter>",
    "apk add openssh<enter><wait5s>",
    "echo PermitRootLogin yes >>/etc/ssh/sshd_config<enter>",
    "service sshd start<enter>"
  ]
}

build {
  sources = ["source.qemu.alpine"]
  provisioner "file" {
    source = "answerfile"
    destination = "answerfile"
  }
  provisioner "file" {
    source = "setup_in_chroot.sh"
    destination = "setup_in_chroot.sh"
  }
  provisioner "file" {
    source = "setup_more.sh"
    destination = "setup_more.sh"
  }
  provisioner "file" {
    source = "../resources/motd"
    destination = "motd"
  }
  provisioner "file" {
    source = "../resources/cloud-set-guest-sshkey.in"
    destination = "cloud-set-guest-sshkey.in"
  }
  provisioner "file" {
    source = "../resources/cloud-set-guest-sshkey.rc"
    destination = "cloud-set-guest-sshkey.rc"
  }
  provisioner "file" {
    source = "grow_disk_size.rc"
    destination = "grow_disk_size.rc"
  }
  #provisioner "file" {
  #  source = "../resources/99_cloudstack.cfg"
  #  destination = "99_cloudstack.cfg"
  #}
  provisioner "shell" {
    inline = [
      "setup-alpine -e -f answerfile",
      "mkdir /mnt/sys",
      "mount /dev/vda3 /mnt/sys",
      "cp setup_in_chroot.sh /mnt/sys",
      "cp setup_more.sh /mnt/sys",
      "cp motd /mnt/sys",
      "cp cloud-set-guest-sshkey.in /mnt/sys",
      "cp cloud-set-guest-sshkey.rc /mnt/sys",
      "cp grow_disk_size.rc /mnt/sys",
#      "cp 99_cloudstack.cfg /mnt/sys",
      "chroot /mnt/sys sh /setup_in_chroot.sh"
    ]
  }
}
