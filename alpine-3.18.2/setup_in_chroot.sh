set -ex

# docker, sudo are in community repositories
sed -i /community/s/^#// /etc/apk/repositories
# shadow gives useradd and usermod
# git and gcompat are needed by gitlab-runner
# nfs-utils is needed for NFS cache sharing
# e2fsprogs-extra is needed for resize2fs
apk add --update docker openrc sudo curl bash shadow git gcompat nfs-utils \
  e2fsprogs-extra

# ci home is /builds
mv ~ci /builds
usermod -d /builds ci

# set ci password for ci
# (I don't know how to set a password non-interactively with setup-alpine)
echo ci:ci | chpasswd

# run docker service at boot time
rc-update add docker boot

# wheel is sudoer without password
echo "%wheel ALL=(ALL:ALL) NOPASSWD: ALL" >/etc/sudoers.d/99-wheel-nopasswd

# gitlab-runner needs /proc to be mounted
mount -t proc proc /proc

# install gitlab-runner manually
# https://docs.gitlab.com/runner/install/linux-manually.html
curl -L --output /usr/local/bin/gitlab-runner \
    "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
chmod +x /usr/local/bin/gitlab-runner

# we set uid/gid for gitlab-runner to 995/995 to match Ubuntu defaults
groupadd gitlab-runner --gid 995
useradd --comment 'GitLab Runner' --create-home gitlab-runner \
    --shell /bin/bash --uid 995 --gid 995
gitlab-runner install --user=gitlab-runner \
    --working-directory=/home/gitlab-runner

# ci and gitlab-runner are allowed to run docker
addgroup ci docker
addgroup gitlab-runner docker

# /cache directory for gitlab-runner
mkdir /cache
chown ci /cache

# setup motd
chown root:root motd
chmod 644 motd
mv motd /etc/motd

# setup grow disk size service for the next boot
chown root:root grow_disk_size.rc
chmod 744 grow_disk_size.rc
mv grow_disk_size.rc /etc/init.d/grow_disk_size
rc-update add grow_disk_size default

# setup more
sh ./setup_more.sh

# remove ssh keys: they will be regenerated on next reboot (typically,
# on the first boot of VMs created with the generated template)
rm /etc/ssh/ssh_host_*

# clean up ourself
rm /setup_in_chroot.sh
rm /setup_more.sh

# syncing before halting
sync
