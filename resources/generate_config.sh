if [ -z "$iso_image_name" ]; then
    iso_image_name="$image_name"
fi

cat <<EOF
validate:
  tags: [small]
  image:
    name: hashicorp/packer
    entrypoint: ["/bin/bash", "-c"]
  script:
    - cd $image_name
    - |
      if [ -f generate_template.sh ]; then
        sh generate_template.sh
      fi
    - packer init .
    - packer validate .

fmt:
  tags: [small]
  image:
    name: hashicorp/packer
    entrypoint: ["/bin/bash", "-c"]
  script:
    - cd $image_name
    - |
      if [ -f generate_template.sh ]; then
        sh generate_template.sh
      fi
    - packer fmt .

download:
  cache:
    key: $iso_image_name.iso
    paths:
      - $iso_image_name/$iso_image_name.iso
  tags: [packer]
  script:
    - |
      if [ ! -f $iso_image_name/$iso_image_name.iso ]; then
        wget --output-document $iso_image_name/$iso_image_name.iso $url
      fi

build:
  needs: [download]
  cache:
    - key: $iso_image_name.iso
      paths:
        - $iso_image_name/$iso_image_name.iso
  tags: [packer]
  script:
    - |
      ( cd $image_name &&
        if [ -f http/user-data ]; then
          cloud-init schema --config-file http/user-data;
        fi &&
        if [ -f generate_template.sh ]; then
          sh generate_template.sh
        fi &&
        packer init . &&
        packer build .
      )
    - mkdir -p ~/www/"$TEMPLATE_PREFIX"/
    - mv "$image_name"/output-*/"$image_name.qcow2" ~/www/"$TEMPLATE_PREFIX/$image_name.qcow2"
    - ips=(\$(hostname -I))
    - |
      if [ -n "$CLOUDSTACK_API_KEY" ]; then
        python3 resources/register-template.py \
          "$template_name" "$display_text" "$os_type" \
          "http://\${ips[0]}/gitlab-runner/$TEMPLATE_PREFIX/$image_name.qcow2" \
          $REGISTER_FLAGS
      fi
EOF
