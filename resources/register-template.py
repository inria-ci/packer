import argparse
import cs
import logging
import os
import time

PROGRAM_NAME = "register-template"


logging.basicConfig(level=logging.INFO, format="%(asctime)s | %(message)s")


def handle_commandline():
    parser = argparse.ArgumentParser(
        prog=PROGRAM_NAME, description="Register a template on ci.inria.fr"
    )
    parser.add_argument("name", help="template name")
    parser.add_argument("displaytext", help="display text")
    parser.add_argument("os_type", help="OS type")
    parser.add_argument("url", help="template URL")
    parser.add_argument(
        "--public",
        action="store_true",
        help="community template"
    )
    args = parser.parse_args()
    cloudstack = cs.CloudStack(
        endpoint="https://sesi-cloud-ctl1.inria.fr/client/api",
        key=os.environ["CLOUDSTACK_API_KEY"],
        secret=os.environ["CLOUDSTACK_SECRET_KEY"],
    )
    zoneci_list = cloudstack.listZones(name="zone-ci", fetch_list=True)
    zoneid = unwrap_singleton(zoneci_list)["id"]
    ostype_list = cloudstack.listOsTypes(description=args.os_type, fetch_list=True)
    ostypeid = unwrap_singleton(ostype_list)["id"]
    old_templates = cloudstack.listTemplates(
        templatefilter="self", name=args.name, fetch_list=True
    )
    logging.info(f"Registering template {args.name} from {args.url}")
    response = cloudstack.registerTemplate(
        displaytext=args.displaytext,
        name=args.name,
        format="QCOW2",
        hypervisor="KVM",
        zoneid=zoneid,
        url=args.url,
        ispublic=args.public,
        isextractable=True,
        ostypeid=ostypeid,
    )
    template_id = response["template"][0]["id"]
    while True:
        time.sleep(10)
        template_list = cloudstack.listTemplates(templatefilter="self", id=template_id, fetch_list=True)
        template = unwrap_singleton(template_list)
        print(template["status"])
        if template["status"] == "HTTP Server does not support partial get":
            raise Exception("HTTP Server does not support partial get")
        if template["isready"]:
            break
    for old_template in old_templates:
        logging.info(f"Deleting previous template {old_template['id']}")
        cloudstack.deleteTemplate(id=old_template["id"], fetch_result=True)


def unwrap_singleton(l):
    assert len(l) == 1
    return l[0]


if __name__ == "__main__":
    handle_commandline()
