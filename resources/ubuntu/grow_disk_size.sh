#!/bin/sh
set -ex

## I tried to use sfdisk, which looks more suitable for automation,
## but sfdisk does not let me change the last LBA.
#sudo sfdisk --force -d /dev/vda >vda.sfdisk
#actual_disk_size=$(sudo blockdev --getsize /dev/vda)
#current_partition_size=$(sudo blockdev --getsize /dev/vda3)
#last_lba=$(grep last-lba vda.sfdisk | cut -d ' ' -f 2)
#new_last_lba=$(($actual_disk_size - 1))
#new_size=$(($current_partition_size + $new_last_lba - $last_lba))
#sed -i "s/last-lba: .*\$/last-lba: $new_last_lba/" vda.sfdisk
#sed -i "/\/dev\/vda3/s/size=[^,]*,/size=$new_size,/" vda.sfdisk
#sudo sfdisk --force /dev/vda < vda.sfdisk
#rm vda.sfdisk

#d# delete
# # last partition
#n# new
# # last partition
# # default first sector
# # default last sector
#w# write

/bin/echo -e "d\n\nn\n\n\n\nw\n" | fdisk /dev/vda
pvresize /dev/vda3
lvm lvextend -l +100%FREE /dev/ubuntu-vg/ubuntu-lv
resize2fs /dev/mapper/ubuntu--vg-ubuntu--lv
systemctl disable grow_disk_size.service
